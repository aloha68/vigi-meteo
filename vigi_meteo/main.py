from dataclasses import dataclass, field
from typing import List

import aioxmpp
import asyncio
import locale
import logging
import systemd.daemon
import systemd.journal
import vigilancemeteo

from aloha68_config import YamlConfig as Config

logging.basicConfig(level=logging.WARNING)

logger = logging.getLogger("vigi-meteo")
logger.setLevel(logging.DEBUG)
logger.addHandler(systemd.journal.JournalHandler())


@dataclass
class AlertsGroup:
    color: str
    flag: str
    alerts: List[str] = field(default_factory=list)


def get_message_from_weather_alerts(wa: vigilancemeteo.DepartmentWeatherAlert):
    msg = f"{wa.bulletin_date:%A %d %B (%H:%M)} :".capitalize()
    msg += "\nPlus d'infos sur https://vigilance.meteofrance.fr/\n"

    alerts_groups = [
        AlertsGroup(color="Vert", flag="🟢"),
        AlertsGroup(color="Jaune", flag="🟡"),
        AlertsGroup(color="Orange", flag="🟠"),
        AlertsGroup(color="Rouge", flag="🔴"),
    ]

    # On regroupe les alertes
    for alert, color in wa.alerts_list.items():
        for group in alerts_groups:
            if group.color == color:
                group.alerts.append(alert)
                break

    # Affichage de chaque groupe d'alertes
    for group in alerts_groups:
        if group.color != "Vert" and group.alerts:
            msg = f"{msg}\n  {group.flag} {', '.join(group.alerts)}"

    return msg


class WeatherAlertBot:
    """
    Robot permettant d'interroger le service Météo France concernant les vigilances météorologiques
    et qui envoie le résumé de manière régulière dans un salon le discussion XMPP.
    """

    jid: str
    room_jid: str
    client: aioxmpp.PresenceManagedClient
    muc: aioxmpp.MUCClient
    room: aioxmpp.muc.Room

    def __init__(self, conf: Config):
        self.nick = conf.xmpp.nick
        self.jid = aioxmpp.JID.fromstr(f"{conf.xmpp.jid}/Test")
        self.room_jid = aioxmpp.JID.fromstr(conf.xmpp.room)
        self.waiting_time = conf.get("waiting_time", 3600)

        # Création des objets xmpp
        self.client = aioxmpp.PresenceManagedClient(self.jid, aioxmpp.make_security_layer(conf.xmpp.password))
        self.muc = self.client.summon(aioxmpp.MUCClient)

        # Création du service d'alertes
        self.weather_alerts = vigilancemeteo.DepartmentWeatherAlert(str(conf.departement))

    def send_group_message(self, msg: str):
        """ Envoie un message au groupe de discussion """
        message = aioxmpp.Message(type_=aioxmpp.MessageType.GROUPCHAT)
        message.body[None] = msg
        self.room.send_message(message)

    def send_data(self):
        """ Envoie les données d'alerte météo au groupe de discussion """
        msg = get_message_from_weather_alerts(self.weather_alerts)
        self.send_group_message(msg)

    async def start(self):
        """ Démarre le robot """

        async with self.client.connected():
            logger.debug(f"Connecté en tant que {self.client.local_jid}")
            systemd.daemon.notify("READY=1")

            self.room, fut = self.muc.join(self.room_jid, self.nick, autorejoin=True)
            await fut

            logger.debug(f"Connecté au groupe {self.room.jid}")

            # On envoie une première fois le lot de données
            self.send_data()
            latest_bulletin = self.weather_alerts.bulletin_date

            while True:
                await asyncio.sleep(self.waiting_time)

                self.weather_alerts.update_department_status()

                # Si le bulletin est déjà envoyé
                if self.weather_alerts.bulletin_date <= latest_bulletin:
                    logger.debug("Bulletin déjà affiché")
                    continue

                latest_bulletin = self.weather_alerts.bulletin_date
                self.send_data()


async def astart():
    conf = Config("vigi-meteo")
    bot = WeatherAlertBot(conf)
    await asyncio.gather(asyncio.create_task(bot.start()))


def test_output():
    wa = vigilancemeteo.DepartmentWeatherAlert("67")
    msg = get_message_from_weather_alerts(wa)
    print(msg)


def main():
    locale.setlocale(locale.LC_TIME, "fr_FR.utf8")
    # return test_output()
    asyncio.run(astart())
