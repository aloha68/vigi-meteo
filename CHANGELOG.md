# Changelog

Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

Le format est basé sur [Keey a Changelog](https://keepachangelog.com/fr/1.0.0/) et ce projet adhère à la [gestion sémantique de version](https://semver.org/lang/fr/).

## [Non publié]

## [0.2.0] - 2021-07-13

### Ajout
- Écriture d'une procédure d'installation, d'exécution et de mise à jour
- systemd: Dépendance au paquet [systemd-python](https://github.com/systemd/python-systemd)
- systemd: Transfert des logs applicatifs à journalctl
- systemd: Possibilité d'utiliser un service de type *notify* dans le fichier *~/.config/systemd/user/vigi-meteo.service*

### Changement
- Renommage de la commande en *vigi-meteo*
- Configuration: la valeur de la clé *departement* peut maintenant être un entier

## [0.1.0] - 2021-07-10

### Ajout
- Fonctionnalité principale : un robot se connecte à un groupe de discussion XMPP et y affiche les vigilances météos
- Lecture d'un fichier de conf au format yaml

[Non publié]: https://gitlab.com/aloha68/vigi-meteo/-/compare/0.2.0...main
[0.2.0]: https://gitlab.com/aloha68/vigi-meteo/-/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/aloha68/vigi-meteo/-/tags/0.1.0
