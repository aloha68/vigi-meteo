# Vigilance météo

Un robot qui interroge le site vigilance.meteofrance.fr et qui publie les alertes sur un groupe de discussion XMPP

## Installation

Avant toute chose, certains paquets python requierent des dépendances aditionnelles (notamment le paquet [cryptography](https://cryptography.io/en/latest/installation/))

    apt install build-essential libssl-dev libffi-dev python3-dev cargo
    apt install libxml2-dev libxslt1-dev

Quand le dépôt est cloné, depuis le répertoire du projet, il faut tout d'abord installer les dépendances

    poetry install

## Configuration

Dans le fichier .vigi-meteo.conf (soit dans le répertoire courant, soit dans XDG_CONFIG_HOME)

    departement: 67
    waiting_time: 3600

    xmpp:
        jid: robot@server.xmpp
        password: robot_password
        room: robot-chatroom@chat.server.xmpp
        nick: Robot nickname

## Exécution

    poetry run vigi-meteo

## Service systemd

Un fichier d'exemple est placé dans le dépôt. Attention, il est impératif de mettre à jour ce fichier, les divers emplacements doivent pointer au bon endroit. 

    cp vigi-meteo.service ~/.config/systemd/user/
    systemctl --user start vigi-meteo
    systemctl --user enable vigi-meteo

## Mise à jour

Depuis le répertoire d'installation :

    git pull
    poetry update
    systemctl --user restart vigi-meteo
